require('./bootstrap')
import Vue from 'vue'
// Route information for Vue Router
import Routes from '@/js/routers.js'
// Implement root view
import App from '@/js/views/App'
// import store from vuex
import store from './store'
// import layouts here
import { DefaultLayout, MainLayout } from '@/js/layouts'
// register components
Vue.component('default-layout', DefaultLayout)
Vue.component('main-layout', MainLayout)

Vue.config.productionTip = false
const app = new Vue({
  el: '#app',
  router: Routes,
  render: h => h(App),
  store,
})

export default app
