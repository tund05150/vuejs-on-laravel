import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = () => ({
  count: 0,
  posts: []
})

const getters = {
  count: state => state.count,
  posts: state => state.posts
}

const mutations = {
  SET_COUNT: state => {
    state.count = state.count + 1
  },
  CREATE_POST: (state, post) => {
    state.posts.unshift(post)
  },
  FETCH_POSTS: (state, posts) => {
    return state.posts = posts
  },
  DELETE_POST: (state, post) => {
    let index = state.posts.findIndex(item => item.id === post.id)
    state.posts.splice(index, 1)
  }
}

const actions = {
  setCount: ({ commit }) => {
    commit('SET_COUNT')
  },
  createPost({commit}, post) {
    axios.post('/api/posts', post)
      .then(res => {
        commit('CREATE_POST', res.data)
      }).catch(err => {
      console.log(err)
    })

  },
  fetchPosts({commit}) {
    axios.get('/api/posts')
      .then(res => {
        commit('FETCH_POSTS', res.data)
      }).catch(err => {
      console.log(err)
    })
  },
  deletePost({commit}, post) {
    axios.delete(`/api/posts/${post.id}`)
      .then(res => {
        if (res.data === 'ok')
          commit('DELETE_POST', post)
      }).catch(err => {
      console.log(err)
    })
  }
}

const store = new Vuex.Store({
  state,
  getters,
  mutations,
  actions
})

export default store
