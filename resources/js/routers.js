import Vue from 'vue'
import VueRouter from 'vue-router'

import {
  HomePage,
  AboutPage,
  UserPage,
  PostPage
} from '@/js/pages'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  routes: [{
      path: '/',
      name: 'home',
      meta: {
        layout: 'main'
      },
      component: HomePage
    },
    {
      path: '/about',
      name: 'about',
      meta: {
        layout: 'main'
      },
      component: AboutPage
    },
    {
      path: '/users',
      name: 'user',
      meta: {
        layout: 'main'
      },
      component: UserPage
    },
    {
      path: '/posts',
      name: 'posts',
      meta: {
        layout: 'main'
      },
      component: PostPage
    }
  ]
})

export default router
