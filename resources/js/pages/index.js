export { default as HomePage } from './Home'

export { default as AboutPage } from './About'

export { default as UserPage } from './User'

export { default as PostPage } from './Post'
